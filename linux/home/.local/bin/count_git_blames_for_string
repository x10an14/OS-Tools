#!/usr/bin/env python3.7

import collections
import itertools
import json
import pathlib
import subprocess
import sys
import typing

def ensure_in_git_repo(path: pathlib.Path) -> bool:
    current_dir = path.resolve(strict=True)

    for path in itertools.chain([current_dir], current_dir.parents):
        if not path.is_dir():
            continue
        if any(
            f.name == '.git'
            for f in path.iterdir()
            if f.is_dir()
        ):
            return True
    else:
        return False

def get_matching_commits(regexp: str) -> typing.List[str]:
    # git log -G'docker rm -f' --pretty=format:"%h%x09%an"
    result = subprocess.run(
        [
            'git',
            'log',
            f"-G{regexp}",
            '--pretty=format:"%h%x09%an"'
        ],
        capture_output=True,
        check=True,
        text=True,
    )
    return list(
        tuple(reversed(line.split('\t')))
        for line in result.stdout.replace('"', '').splitlines()
        if line
    )

if __name__ == '__main__':
  if len(sys.argv) < 2:
    print(f"'{sys.argv[0]}' needs a regexp string as input!", file=sys.stderr)
    sys.exit(1)

  assert(ensure_in_git_repo(pathlib.Path.cwd()))
  result = get_matching_commits(regexp=sys.argv[1])
  print(
    json.dumps(
      collections.Counter(x[0] for x in result),
      indent=2,
    )
  )
