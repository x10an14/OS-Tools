#!/usr/bin/env bash

LOGFOLDER="/var/log/i3"

if [ ! -d "${LOGFOLDER}" ]; then
    mkdir "$(realpath "${LOGFOLDER}")"
fi
LOGFILE=$(basename "${0}")
LOGFILE="${LOGFOLDER}/${LOGFILE}.log"
