#!/usr/bin/env python3
from itertools import chain
import json
from pathlib import Path
import sys
from typing import List


def get_all_folders(path: Path) -> List[Path]:
    if not path.is_dir(): return []
    if any(f for f in path.iterdir() if f.is_dir()) is False:
        return [path]
    folders = []
    for f in path.iterdir():
        if not f.is_dir(): continue
        folders += get_all_folders(f)
    return folders


if __name__ == '__main__':
    git_home_folder = Path(__file__).parent / 'home'
    list_of_folders = get_all_folders(git_home_folder)
    list_of_files = list( 
        filter(
            lambda f: f.is_file(),
            chain.from_iterable(
                map(
                    lambda d: d.iterdir(),
                    list_of_folders
                )
            )
        )
    )

    # print(json.dumps([str(d) for d in list_of_folders], indent=2))
    # print()
    # print(json.dumps([str(f) for f in list_of_files], indent=2), end='\n\n')
    # sys.exit(0)
    home_folder = Path.home()
    symlinks = {
        Path(
            home_folder / str(target).lstrip(f"{git_home_folder}/")
        ): target.resolve(strict=True)
        for target in list_of_files
    }
    # print(symlinks, end='\n\n')
    # sys.exit(0)
    for symlink, target in symlinks.items():
        # print(symlink, symlink)
        # Ensure folder exists
        symlink.parent.mkdir(parents=True, exist_ok=True)
        if symlink.is_symlink():
            # Update any existing symlinks, like if git-repo moves
            # print(f"\t'{symlink}' will be overwritten!", file=sys.stderr)
            symlink.unlink()
        if symlink.exists():
            # 'symlink' Path object points to non-symlink filesystem item
            answer = input(f"'{symlink}' exists, overwrite [Y/n]? ")
            if answer and not(answer.lower().startswith('y')): continue
        # print(f"\tSymlinking {symlink} => {target.resolve()}", file=sys.stderr)
        symlink.symlink_to(target.resolve())
